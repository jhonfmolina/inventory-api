<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mariachis</title>
</head>
<body>
Hola <strong>{{ $name }}</strong>,

<hr>

nombre del servicio : <span>{{ $servicio_nombre }}</span>
<hr>
valor del servicio : <span>{{ $servicio_valor }}</span>
<hr>
nombre del cliente : <span>{{ $cliente_nombre }}</span>
<hr>
telefono del cliente : <span>{{ $telefono }}</span>
<hr>
hora del evento : <span>{{ $hora }}</span>
<hr>
fecha del evento : <span>{{ $fecha }}</span>
<hr>
direccion del evento : <span>{{ $direccion }}</span>
</body>
</html>
