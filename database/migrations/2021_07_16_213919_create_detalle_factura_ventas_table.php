<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetalleFacturaVentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_factura_ventas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('factura_venta_id')
                ->constrained('factura_ventas');
            $table->foreignId('articulo_id')
                ->constrained('articulos');
            $table->string('concepto_articulo');
            $table->integer('contidad');
            $table->decimal('precio_unitario', 14,4);
            $table->decimal('descuento', 14,4);
            $table->decimal('iva', 14,4);
            $table->decimal('sub_total', 14,4);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_factura_ventas');
    }
}
