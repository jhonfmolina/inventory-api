<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetalleFacturaComprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_factura_compras', function (Blueprint $table) {
            $table->id();
            $table->foreignId('factura_compra_id')
                ->constrained('factura_compras');
            $table->foreignId('articulo_id')
                ->constrained('articulos');
            $table->string('concepto_articulo');
            $table->integer('contidad');
            $table->decimal('precio_unitario', 14,4);
            $table->decimal('descuento', 14,4);
            $table->decimal('iva', 14,4);
            $table->decimal('sub_total', 14,4);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_factura_compras');
    }
}
