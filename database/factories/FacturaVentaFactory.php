<?php

namespace Database\Factories;

use App\Models\FacturaVenta;
use Illuminate\Database\Eloquent\Factories\Factory;

class FacturaVentaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = FacturaVenta::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
