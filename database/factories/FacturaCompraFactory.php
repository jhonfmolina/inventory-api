<?php

namespace Database\Factories;

use App\Models\FacturaCompra;
use Illuminate\Database\Eloquent\Factories\Factory;

class FacturaCompraFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = FacturaCompra::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
