<?php

namespace Database\Factories;

use App\Models\DetalleFacturaCompra;
use Illuminate\Database\Eloquent\Factories\Factory;

class DetalleFacturaCompraFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DetalleFacturaCompra::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
