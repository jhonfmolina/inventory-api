<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FacturaVenta extends Model
{
    use HasFactory;
    protected $fillable = [
        'fecha_emision',
        'cliente_id',
        'valor_total',
        'valor_total_iva',
        'valor_total_descuento'
    ];
}
