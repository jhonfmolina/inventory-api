<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Articulo extends Model
{
    use HasFactory;
    protected $fillable = [
        'nombre',
        'marca',
        'codigo',
        'descripcion',
        'precio_costo',
        'iva',
        'stock'
    ];
}
