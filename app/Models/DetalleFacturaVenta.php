<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetalleFacturaVenta extends Model
{
    use HasFactory;
    protected $fillable = [
        'factura_venta_id',
        'articulo_id',
        'concepto_articulo',
        'contidad',
        'precio_unitario',
        'descuento',
        'iva',
        'sub_total'
    ];
}
