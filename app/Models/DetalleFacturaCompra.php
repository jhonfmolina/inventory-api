<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetalleFacturaCompra extends Model
{
    use HasFactory;
    protected $fillable = [
        'factura_compra_id',
        'articulo_id',
        'concepto_articulo',
        'contidad',
        'precio_unitario',
        'descuento',
        'iva',
        'sub_total'
    ];
}
