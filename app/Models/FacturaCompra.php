<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FacturaCompra extends Model
{
    use HasFactory;
    protected $fillable = [
        'fecha_emision',
        'proveedor_id',
        'valor_total',
        'valor_total_iva',
        'valor_total_descuento'
    ];
}
