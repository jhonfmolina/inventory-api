<?php

namespace App\Http\Controllers;

use App\Models\Proveedor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ProveedorController extends Controller
{
    public function index()
    {
        $response = Proveedor::all();
        return response()->json([
            'mensaje' => 'listado de recursos.',
            'data' => $response
        ]);
    }

    public function store(Request $request)
    {
        $valida = Validator::make($request->all(), [
            'nombres' => 'required',
            'apellidos' => 'required',
            'tipo_documento' => 'required',
            'documento' => [
                'required',
                Rule::unique('proveedors')
            ],
            'correo' => 'present',
            'contacto' => 'present',
            'direccion' => 'present'
        ])->validate();

        $response = Proveedor::create($valida);
        return response()->json([
            'mensaje' => 'recurso creado exitosamente.',
            'data' => $response
        ]);
    }
    public function show(Proveedor $proveedore)
    {
        return response()->json([
            'mensaje' => 'recurso',
            'data' => $proveedore
        ]);
    }

    public function update(Request $request, Proveedor $proveedore)
    {
        $valida = Validator::make($request->all(), [
            'nombres' => 'required',
            'apellidos' => 'required',
            'tipo_documento' => 'required',
            'documento' => 'required',
            'correo' => 'present',
            'contacto' => 'present',
            'direccion' => 'present'
        ])->validate();
        Proveedor::where('id', $proveedore->id)->update($valida);
        return response()->json([
            'mensaje' => 'recurso actualizado exitosamente.',
            'data' => true
        ]);
    }
}
