<?php

namespace App\Http\Controllers;

use App\Models\Articulo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ArticuloController extends Controller
{
    public function index()
    {
        $response = Articulo::all();
        return response()->json([
            'mensaje' => 'listado de recursos.',
            'data' => $response
        ]);
    }

    public function store(Request $request)
    {
        $valida = Validator::make($request->all(), [
            'nombre' => 'required',
            'marca' => 'required',
            'codigo' => [
                'required',
                Rule::unique('articulos')
            ],
            'descripcion' => 'required',
            'precio_costo' => 'present',
            'iva' => 'required'
        ])->validate();

        $response = Articulo::create($valida);
        return response()->json([
            'mensaje' => 'recurso creado exitosamente.',
            'data' => $response
        ]);
    }
    public function show(Articulo $articulo)
    {
        return response()->json([
            'mensaje' => 'recurso',
            'data' => $articulo
        ]);
    }

    public function update(Request $request, Articulo $articulo)
    {
        $valida = Validator::make($request->all(), [
            'nombre' => 'required',
            'marca' => 'required',
            'codigo' => 'required',
            'descripcion' => 'required',
            'precio_costo' => 'present',
            'iva' => 'present'
        ])->validate();
        Articulo::where('id', $articulo->id)->update($valida);
        return response()->json([
            'mensaje' => 'recurso actualizado exitosamente.',
            'data' => true
        ]);
    }
}
