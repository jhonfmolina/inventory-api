<?php

namespace App\Http\Controllers;

use App\Models\Articulo;
use App\Models\DetalleFacturaCompra;
use App\Models\FacturaCompra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class FacturaCompraController extends Controller
{

    public function store(Request $request)
    {
        DB::transaction(function () use ($request){
            $valida = Validator::make($request->all(), [
                'fecha_emision' => 'required|date',
                'proveedor_id' => [
                    'required',
                    Rule::exists('proveedors', 'id')
                ],
                'valor_total' => 'required|integer',
                'valor_total_iva' => 'required|integer',
                'valor_total_descuento' => 'required|integer',
                'detalle' => 'required|array',
                'detalle.*.articulo_id' => [
                    'required',
                    Rule::exists('articulos', 'id')
                ],
                'detalle.*.concepto_articulo' => 'required',
                'detalle.*.contidad' => 'required|integer',
                'detalle.*.precio_unitario' => 'required|integer',
                'detalle.*.descuento' => 'required|integer',
                'detalle.*.iva' => 'required|integer',
                'detalle.*.sub_total' => 'required|integer'
            ])->validate();

            $factura = FacturaCompra::create(array(
                'fecha_emision' => $valida['fecha_emision'],
                'proveedor_id' => $valida['proveedor_id'],
                'valor_total' => $valida['valor_total'],
                'valor_total_iva' => $valida['valor_total_iva'],
                'valor_total_descuento' => $valida['valor_total_descuento']
            ));

            foreach ($valida['detalle'] as $item)
            {
                DetalleFacturaCompra::create(array(
                    'factura_compra_id' => $factura->id,
                    'articulo_id' => $item['articulo_id'],
                    'concepto_articulo' => $item['concepto_articulo'],
                    'contidad' => $item['contidad'],
                    'precio_unitario' => $item['precio_unitario'],
                    'descuento' => $item['descuento'],
                    'iva' => $item['iva'],
                    'sub_total' => $item['sub_total']
                ));

                $articulo = Articulo::where('id', $item['articulo_id'])->first();
                $articulo->stock += $item['contidad'];
                $articulo->save();
            }

        });
        return response()->json([
            'mensaje' => 'recurso creado exitosamente.',
            'data' => true
        ]);
    }

}
