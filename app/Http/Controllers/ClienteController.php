<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ClienteController extends Controller
{

    public function index()
    {
        $response = Cliente::all();
        return response()->json([
            'mensaje' => 'listado de recursos.',
            'data' => $response
        ]);
    }

    public function store(Request $request)
    {
        $valida = Validator::make($request->all(), [
            'nombres' => 'required',
            'apellidos' => 'required',
            'tipo_documento' => 'required',
            'documento' => [
                'required',
                Rule::unique('clientes')
            ],
            'correo' => 'present',
            'contacto' => 'present',
            'direccion' => 'present'
        ])->validate();

        $response = Cliente::create($valida);
        return response()->json([
            'mensaje' => 'recurso creado exitosamente.',
            'data' => $response
        ]);
    }
    public function show(Cliente $cliente)
    {
        return response()->json([
            'mensaje' => 'recurso',
            'data' => $cliente
        ]);
    }

    public function update(Request $request, Cliente $cliente)
    {
        $valida = Validator::make($request->all(), [
            'nombres' => 'required',
            'apellidos' => 'required',
            'tipo_documento' => 'required',
            'documento' => 'required',
            'correo' => 'present',
            'contacto' => 'present',
            'direccion' => 'present'
        ])->validate();
        Cliente::where('id', $cliente->id)->update($valida);
        return response()->json([
            'mensaje' => 'recurso actualizado exitosamente.',
            'data' => true
        ]);
    }
}
